import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PlaceholderService } from './placeholder.service';

@Component
  (
    {
      selector: 'app-root',
      templateUrl: './app.component.html',
      styleUrls: ['./app.component.css']
    }
  )

export class AppComponent implements OnInit {


  // decalre a models for this component as an array
  people
  frmId:number = 1 
  whichCategory = 'people'
  choices:Object[] = [{cat:'people'},{cat:'vehicles'},{cat:'planets'},{cat:'species'},{cat:'starships'}];
  model
  must



  // importing service from placeholder class
  constructor(private placeholderService: PlaceholderService) {

  }

  // handles click events of the button choose
  handleClick()
  {
    this.placeholderService.getParamData(this.frmId, this.whichCategory)
    .subscribe( (result)=>{this.model = result})
    
  }

  // call a method from our service ie placeholder.service, calling getData method
  invokeService() {
    this.placeholderService.getData().subscribe(
      (result) => {
        console.log(result);
        this.people = result;
      })
  }
 
  // this invokes the service
  ngOnInit() {
    this.invokeService()
    
  }

}
